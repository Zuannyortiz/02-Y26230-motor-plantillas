const express = require("express");
const {request, response} = require("express");
const db = require("./db/data");
const hbs = require("hbs");
const {media} = require("./db/data");
const app = express();
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set("views", __dirname + "/views")

hbs.registerPartials(__dirname + "/views/partials");

//Ruta locahost:[3000]
app.get("/", (request, response) => {
    response.render("index", {
        integrantes: db.integrantes,
    });
});
app.get("/curso", (request, response)=>{
    response.render("curso",{
        curso: db.curso[0],
    })
})
app.get("/word_cloud", (request, response)=>{
    response.render("word_cloud")
});
app.get("/logo", (request, response)=>{
    response.render("code_bravo")
});


const matriculas = [...new Set(db.media.map(item => item.matricula))];

app.get("/:matricula", (request, response) => {
    const matricula = request.params.matricula;
    // Se verifica si la matricula existe.
    if (matriculas.includes(matricula)) {
        const mediaFiltrada = db.media.filter(item => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter(item => item.matricula === matricula);
        response.render('integrantes', {
            tipoMedia: db.tipoMedia,
            integrantes: integrantesFiltrados,
            media: mediaFiltrada,
        });
    }
});
app.use((req, res, next) => {
    res.status(404).render('mensaje');
});

app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000");
    console.log("http://localhost:3000/");
});

//console.log ("Base de datos simulada", db);